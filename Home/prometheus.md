# Prometheus Configurations (And helpers)

### Base Iron
- Debian system
- No other account other than root
- Configured through salt

Configuration file
```
global:
  scrape_interval:     15s # By default, scrape targets every 15 seconds.

  # Attach these labels to any time series or alerts when communicating with
  # external systems (federation, remote storage, Alertmanager).
  external_labels:
    monitor: 'codelab-monitor'

# A scrape configuration containing exactly one endpoint to scrape:
# Here it's Prometheus itself.
scrape_configs:
  # The job name is added as a label `job=<job_name>` to any timeseries scraped from this config.
  - job_name: 'prometheus'

    # Override the global default and scrape targets from this job every 5 seconds.
    scrape_interval: 5s

    static_configs:
      - targets: ['localhost:9090']
  - job_name: 'node_exporter_metrics'
    scrape_interval: 5s
    static_configs:
            - targets: ['gusion.lakeyhouse:9100','amon.lakeyhouse:9100', 'saltmaster.lakeyhouse:9100','icinga.lakeyhouse:9100']
```


### Helper script for non packaged systems
```
#!/usr/bin/bash
cd /tmp
curl -LO https://github.com/prometheus/node_exporter/releases/download/v0.18.0/node_exporter-0.18.0.linux-amd64.tar.gz
tar xvf node_exporter*
mv node_exporter*/node_exporter /usr/local/bin/
useradd -rs /bin/false node_exporter
cat << '__EOF__' > /etc/systemd/system/node_exporter.service
[Unit]
Description=Node Exporter
After=network.target

[Service]
User=node_exporter
Group=node_exporter
Type=simple
ExecStart=/usr/local/bin/node_exporter

[Install]
WantedBy=multi-user.target
__EOF__
systemctl daemon-reload
systemctl start node_exporter
systemctl status node_exporter
systemctl enable node_exporter
firewall-cmd --permanent --add-port 9100/tcp
firewall-cmd --reload
rm -rf node_exporter-0.18.0.linux-amd64*
```