# Vagrant configurations

## Libvirt
First things first you need the plugin

- `vagrant plugin install vagrant-libvirt`

Next you need to create your vagrant file(s)

- `vagrant init`

## Vagrant files

This will make a vagrant box of "DEFAULT" box with an allocation of 1GB of RAM.
```
<   config.vm.provider "virtualbox" do |v|
<     v.memory = "1024"
---
>   config.vm.provider "libvirt" do |lv|
>     lv.memory = "1024"
```


Here is my `./Vagrantfile`
```
# -*- mode: ruby -*-
# vi: set ft=ruby :
Vagrant.configure("2") do |config|
  config.vm.provider "libvirt" do |lv|
    lv.memory = "1024"
  end
end
```

## Provisioning
This example will start up a base box with 1GB of RAM and be provisioned by the script provision.bash.
The naming of the script does not matter as long it's reflected in the Vagrantfile
```
# -*- mode: ruby -*-
# vi: set ft=ruby :
Vagrant.configure("2") do |config|
  config.vm.provider "libvirt" do |vb|
    vb.memory = "1024"
  end
  config.vm.provision "shell", path: "./provision.bash"
end
```